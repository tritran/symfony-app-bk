<?php

namespace App\Service;

class Calculator
{
    /**
     * @var \App\Service\Sum
     */
    private $sum;

    /**
     * Calculator constructor.
     *
     * @param \App\Service\Sum $sum
     */
    public function __construct(Sum $sum)
    {
        $this->sum = $sum;
    }

    /**
     * @param int $a
     * @param int $b
     *
     * @return int
     */
    public function sum(int $a, int $b): int
    {
        return $this->sum->execute($a, $b);
    }
}
