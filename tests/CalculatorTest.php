<?php

namespace App\Tests;

use App\Service\Calculator;
use App\Service\Sum;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     *
     */
    public function testSum()
    {
        $stub = $this->createMock(Sum::class);

        $stub->method('execute')
            ->willReturn(36);

        $calculator = new Calculator($stub);
        $result = $calculator->sum(10, 26);

        $this->assertEquals($result, 36);
    }

    public function testFailedSum()
    {
        $stub = $this->createMock(Sum::class);

        // Configure the stub.
        $stub->method('execute')
            ->willReturn(36);

        $calculator = new Calculator($stub);
        $result = $calculator->sum(10, 26);

        $this->assertNotEquals($result, 37);
    }
}
