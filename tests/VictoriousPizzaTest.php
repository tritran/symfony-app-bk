<?php

namespace App\Tests;

use App\Entity\VictoriousPizza;
use PHPUnit\Framework\TestCase;

class VictoriousPizzaTest extends TestCase
{
    public function testGetterSetter()
    {
        $pizza = new VictoriousPizza();
        $pizza->setName('Victorious');
        $this->assertEquals('Victorious', $pizza->getName());
    }
}
